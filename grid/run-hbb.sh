#!/usr/bin/env bash


if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

PARENT=$(pwd)


TAGGER_NETWORK="HbbNetwork.json"

AUX_ARGS=''
if [[ -f ${PARENT}/${TAGGER_NETWORK} ]] ; then
    AUX_ARGS+=" -t "${PARENT}/${TAGGER_NETWORK}
fi

# move into build and run!
cd build

IFS=',' read -a INPUTS <<< "$1"

# if this grid node uses local datasets we need to copy these too
for INPUT in ${INPUTS[*]}; do
    if [[ -f ${PARENT}/${INPUT} ]]; then
        ln -f -s ${PARENT}/${INPUT}
    fi
done

echo "RUNNING ON ${INPUTS[*]} with args '${AUX_ARGS}'"

./**/bin/dump-hbb ${INPUTS[*]} ${AUX_ARGS}

echo "moving output file to parent directory"

mv *.h5 $PARENT

echo 'DONE'
