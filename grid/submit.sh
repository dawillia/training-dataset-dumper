#!/usr/bin/env bash

if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

INPUT_DATASETS=(
    mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG2.e6337_e5984_s3126_r9781_r9778_p3446/
    mc16_13TeV.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.deriv.DAOD_FTAG2.e5362_s3126_r9781_r9778_p3446/
)

BASE=training-dataset-dumper

GRID_NAME=${RUCIO_ACCOUNT-${USER}}

echo "adding libhdf5 to submit"
rsync -a /usr/lib*/libhdf5.so.6.* libhdf5.so.6

# make up a unique id for this batch of jobs
BATCH_ID=$(date +%F-T%H%M%S)-R${RANDOM}

ZIP=job.tgz
if [[ -f ${ZIP} ]] ; then
    rm ${ZIP}
fi
echo "making tarball of local files: ${ZIP}" >&2
prun --outTarBall=${ZIP} --noSubmit --exec "ls"\
     --extFile=libhdf5.so.6\
     --outDS user.${GRID_NAME}.x

for DS in ${INPUT_DATASETS[*]}
do
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
    OUT_DS=user.${GRID_NAME}.${DSID}.btagTraining.${BATCH_ID}
    echo "Submitting for ${GRID_NAME} on ${DS} -> ${OUT_DS}"
    prun --bexec "${BASE}/grid/build.sh" --exec "${BASE}/grid/run.sh %IN"\
         --outDS ${OUT_DS} --inDS ${DS}\
         --outputs output.h5,output.root\
         --inTarBall=${ZIP}\
         --athenaTag=AnalysisBase,21.2.40
done
