#!/usr/bin/env bash

if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
    set -eu
fi

GRID_NAME=${RUCIO_ACCOUNT-${USER}}
BASE=training-dataset-dumper
INPUT_DATASETS=( $(cat ${BASE}/datasets/ftag5-*) )

# prep submit area
echo "preping submit area"
if [[ -d submit ]]; then
    echo "removing old submit directory"
    rm -rf submit
fi
mkdir submit
if [[ -d athena ]]; then
    echo "copying athena into submit dir"
    rsync -a athena submit/ --exclude=.git
fi
NETWORK="HbbNetwork.json"
if [[ -f $NETWORK ]]; then
    echo "copying network into submit dir"
    cp $NETWORK submit/
fi
echo "copying ${BASE} into submit directory"
cd submit
cp -r ../${BASE} .
echo "adding libhdf5 to submit"
rsync -a /usr/lib*/libhdf5.so.6.* libhdf5.so.6

# make up a unique id for this batch of jobs
BATCH_ID=$(date +%F-T%H%M%S)-R${RANDOM}
# BATCH_ID=p3

ZIP=job.tgz
if [[ -f ${ZIP} ]] ; then
    rm ${ZIP}
fi
echo "making tarball of local files: ${ZIP}" >&2
prun --outTarBall=${ZIP} --noSubmit --exec "ls"\
     --extFile=$NETWORK,libhdf5.so.6\
     --outDS user.${GRID_NAME}.x


for DS in ${INPUT_DATASETS[*]}
do
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
    OUT_DS=user.${GRID_NAME}.${DSID}.hbbTraining.${BATCH_ID}
    echo "Submitting for ${GRID_NAME} on ${DS} -> ${OUT_DS}"
    set +e
    prun --bexec "${BASE}/grid/build.sh"\
         --exec ". ${BASE}/grid/prerun-hbb.sh; ${BASE}/grid/run-hbb.sh %IN"\
         --outDS ${OUT_DS} --inDS ${DS}\
         --outputs output.h5\
         --noEmail --inTarBall=${ZIP}\
         --athenaTag=AnalysisBase,21.2.27 > ${OUT_DS}.log 2>&1 &
    sleep 2

done
wait
