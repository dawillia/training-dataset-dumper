# this should be sourced to set up some things

ATH_BUILD_DIR=build-athena

echo "Adding local directory to LD_LIBRARY_PATH"
export LD_LIBRARY_PATH=$(pwd):${LD_LIBRARY_PATH}

if [[ -d $ATH_BUILD_DIR ]]; then
    echo "setting up custom athena packages"
    . ${ATH_BUILD_DIR}/**/setup.sh
fi

