#ifndef BTAGGING_WRITER_CONFIGURATION_HH
#define BTAGGING_WRITER_CONFIGURATION_HH

#include <vector>
#include <string>
#include <map>
#include <stdexcept>

namespace cfg {
  typedef std::vector<std::string> List;

  // Fat jets
  const List FatJetFloats = {
    "Split12",
    "Split23",
    "Qw",
    "PlanarFlow",
    "Angularity",
    "Aplanarity",
    "ZCut12",
    "KtDR",
  };

  // BTagging
  const List BTagDoubles = {
    "MV2c10_discriminant",
    "MV2c10mu_discriminant",
    "MV2c10rnn_discriminant",
    "MV2c100_discriminant",
    "MV2cl100_discriminant",
    "DL1_pu", "DL1_pc", "DL1_pb",
    "DL1rnn_pu", "DL1rnn_pc", "DL1rnn_pb",
    "IP2D_pu", "IP2D_pc", "IP2D_pb",
    "IP3D_pu", "IP3D_pc", "IP3D_pb",
    "SV1_pu", "SV1_pc", "SV1_pb",
    "rnnip_pu", "rnnip_pc", "rnnip_pb", "rnnip_ptau",
  };
  const List BTagFloats = {
    "JetFitter_energyFraction",
    "JetFitter_mass",
    "JetFitter_significance3d",
    "JetFitter_deltaphi",
    "JetFitter_deltaeta",
    "JetFitter_massUncorr",
    "JetFitter_dRFlightDir",
    "SV1_masssvx",
    "SV1_efracsvx",
    "SV1_significance3d",
    "SV1_dstToMatLay",
    "SV1_deltaR",
    "SV1_Lxy",
    "SV1_L3d",
  };
  const List BTagInts = {
    "JetFitter_nVTX",
    "JetFitter_nSingleTracks",
    "JetFitter_nTracksAtVtx",
    "JetFitter_N2Tpair",
    "SV1_N2Tpair",
    "SV1_NGTinSvx",
  };
  const List BTagTruthLabels = {
    "GhostBHadronsFinalCount",
    "GhostCHadronsFinalCount",
    "HadronConeExclTruthLabelID",
    "HadronConeExclExtendedTruthLabelID",
  };
  const List BTagTruthFloats = {
    "GhostBHadronsFinalPt",
  };

  // SMT
  const List SMTDoubles = {
    "SMT_discriminant",
  };
  const List SMTFloats = {
    "SMT_mu_pt",
    "SMT_dR",
    "SMT_qOverPratio",
    "SMT_mombalsignif",
    "SMT_scatneighsignif",
    "SMT_pTrel",
    "SMT_mu_d0",
    "SMT_mu_z0",
    "SMT_ID_qOverP",
  };

  const List BTagAddedDoubles = {
    "pt_uncalib",
    "eta_uncalib",
    "abs_eta_uncalib",
    "IP2D_cu", "IP2D_bu", "IP2D_bc",
    "IP3D_cu", "IP3D_bu", "IP3D_bc",
    "secondaryVtx_m",
    "secondaryVtx_E",
    "secondaryVtx_EFrac",
    "secondaryVtx_min_trk_flightDirRelEta",
    "secondaryVtx_max_trk_flightDirRelEta",
    "secondaryVtx_avg_trk_flightDirRelEta",
    "min_trk_flightDirRelEta",
    "max_trk_flightDirRelEta",
    "avg_trk_flightDirRelEta",
  };
  const List BTagAddedFloats = {
    "JetFitter_deltaR",
    "secondaryVtx_L3d",
    "secondaryVtx_Lxy",
  };
  const List BTagAddedInts = {
    "secondaryVtx_nTrks",
  };
  const List BTagAddedChars = {
    "IP2D_isDefaults",
    "IP3D_isDefaults",
    "JetFitter_isDefaults",
    "SV1_isDefaults",
    "SMT_isDefaults",
    "secondaryVtx_isDefaults",
    "rnnip_isDefaults",
  };

  const std::map<std::string, std::vector<std::string> > BTagDefaultsMap = {
    {
      "IP2D_isDefaults",
      {
        "IP2D_pu", "IP2D_pc", "IP2D_pb",
        "IP2D_cu", "IP2D_bu", "IP2D_bc",
      }
    },
    {
      "IP3D_isDefaults",
      {
        "IP3D_pu", "IP3D_pc", "IP3D_pb",
        "IP3D_cu", "IP3D_bu", "IP3D_bc",
      }
    },
    {
      "rnnip_isDefaults",
      {
       "rnnip_pu", "rnnip_pc", "rnnip_pb", "rnnip_ptau",
      }
    },
    {
      "JetFitter_isDefaults",
      {
        "JetFitter_nVTX",
        "JetFitter_nSingleTracks",
        "JetFitter_nTracksAtVtx",
        "JetFitter_N2Tpair",
        "JetFitter_energyFraction",
        "JetFitter_mass",
        "JetFitter_significance3d",
        "JetFitter_deltaphi",
        "JetFitter_deltaeta",
        "JetFitter_massUncorr",
        "JetFitter_dRFlightDir",
        "JetFitter_deltaR",
      }
    },
    {
      "SV1_isDefaults",
      {
        "SV1_NGTinSvx",
        "SV1_N2Tpair",
        "SV1_masssvx",
        "SV1_efracsvx",
        "SV1_significance3d",
        "SV1_dstToMatLay",
        "SV1_deltaR",
        "SV1_Lxy",
        "SV1_L3d",
        "SV1_pu",
        "SV1_pc",
        "SV1_pb",
      }
    },
    {
      "secondaryVtx_isDefaults",
      {
        "secondaryVtx_m",
        "secondaryVtx_E",
        "secondaryVtx_EFrac",
        "secondaryVtx_min_trk_flightDirRelEta",
        "secondaryVtx_max_trk_flightDirRelEta",
        "secondaryVtx_avg_trk_flightDirRelEta",
        "secondaryVtx_L3d",
        "secondaryVtx_Lxy",
        "secondaryVtx_nTrks",
      }
    },
    {
      "SMT_isDefaults",
      {
        "SMT_discriminant",
        "SMT_mu_pt",
        "SMT_dR",
        "SMT_qOverPratio",
        "SMT_mombalsignif",
        "SMT_scatneighsignif",
        "SMT_pTrel",
        "SMT_mu_d0",
        "SMT_mu_z0",
        "SMT_ID_qOverP",
      }
    },
  };

  // Tracks
  const List TrackUChars = {
    "numberOfInnermostPixelLayerHits",
    "numberOfNextToInnermostPixelLayerHits",
    "numberOfPixelHits",
    "numberOfPixelHoles",
    "numberOfPixelSharedHits",
    "numberOfPixelSplitHits",
    "numberOfSCTHits",
    "numberOfSCTHoles",
    "numberOfSCTSharedHits",
  };
  const List TrackInts = {
    "IP2D_grade",
    "IP3D_grade",
  };
  const List TrackFloats = {
    "chiSquared",
    "numberDoF",
    "IP3D_signed_d0",
    "IP2D_signed_d0",
    "IP3D_signed_z0",
    "IP3D_signed_d0_significance",
    "IP3D_signed_z0_significance",
  };

  // utility
  inline void insert_into(std::vector<std::string>& into,
                          const List& inserted) {
    into.insert(into.end(), inserted.begin(), inserted.end());
  }
  inline std::map<std::string, std::string> check_map_from(
    const std::map<std::string, std::vector<std::string> >& default_to_vals){
    std::map<std::string, std::string> out;
    for (const auto& pair: default_to_vals) {
      const std::string& check_var = pair.first;
      for (const auto& var: pair.second) {
        if (out.count(var)) {
          throw std::logic_error(var + " already in map");
        }
        out[var] = check_var;
      }
    }
    return out;
  }
}

#endif
