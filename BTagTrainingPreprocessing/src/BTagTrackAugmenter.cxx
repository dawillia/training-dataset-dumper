#include "BTagTrackAugmenter.hh"

#include <cmath>
#include <cstddef>

#include "xAODJet/Jet.h"

BTagTrackAugmenter::BTagTrackAugmenter():
  m_ip_d0("btag_ip_d0"),
  m_ip_z0("btag_ip_z0"),
  m_ip_d0_sigma("btag_ip_d0_sigma"),
  m_ip_z0_sigma("btag_ip_z0_sigma"),
  m_track_displacement("btag_track_displacement"),
  m_track_momentum("btag_track_momentum"),
  m_ip2d_trackParticleLinks("IP2D_TrackParticleLinks"),
  m_ip3d_trackParticleLinks("IP3D_TrackParticleLinks"),
  m_ip2d_gradeOfTracks("IP2D_gradeOfTracks"),
  m_ip3d_gradeOfTracks("IP3D_gradeOfTracks"),
  m_ip2d_signed_d0("IP2D_signed_d0"),
  m_ip3d_signed_d0("IP3D_signed_d0"),
  m_ip3d_signed_z0("IP3D_signed_z0"),
  m_ip3d_signed_d0_significance("IP3D_signed_d0_significance"),
  m_ip3d_signed_z0_significance("IP3D_signed_z0_significance"),
  m_ip2d_grade("IP2D_grade"),
  m_ip3d_grade("IP3D_grade")
{}

namespace {
  Amg::Vector3D get_vector3d(const std::vector<float> &input_vector) {
    assert(input_vector.size() == 3);
    return Eigen::Vector3f(input_vector.data()).cast<double>();
  }
}

void BTagTrackAugmenter::augment(const xAOD::TrackParticle &track, const xAOD::Jet &jet) {
  const TLorentzVector jet_fourVector = jet.p4();
  const Amg::Vector3D jet_threeVector(jet_fourVector.X(),jet_fourVector.Y(),jet_fourVector.Z());
  const Amg::Vector3D track_displacement = get_vector3d(m_track_displacement(track));
  const Amg::Vector3D track_momentum = get_vector3d(m_track_momentum(track));

  const double ip_d0 = m_ip_d0(track);
  m_ip2d_signed_d0(track) = std::copysign(ip_d0, std::sin(jet_threeVector.phi() - track_momentum.phi()) * ip_d0);
  const double ip3d_signed_d0 = std::copysign(ip_d0, jet_threeVector.cross(track_momentum).dot(track_momentum.cross(-track_displacement)));
  m_ip3d_signed_d0(track) = ip3d_signed_d0;
  m_ip3d_signed_d0_significance(track) = ip3d_signed_d0 / m_ip_d0_sigma(track);

  const double ip_z0 = m_ip_z0(track);
  const double signed_z0 = std::copysign(ip_z0, (jet_threeVector.eta() - track_momentum.eta()) * ip_z0);
  m_ip3d_signed_z0(track) = signed_z0;
  m_ip3d_signed_z0_significance(track) = signed_z0 / m_ip_z0_sigma(track);

  int ip3d_grade = -1;
  const xAOD::BTagging &btagging = *jet.btagging();
  const std::vector<ElementLink<xAOD::TrackParticleContainer> > ip3d_tracks = m_ip3d_trackParticleLinks(btagging);
  for (std::size_t ip3d_track_index = 0; ip3d_track_index < ip3d_tracks.size(); ++ip3d_track_index) {
    if (&track == *(ip3d_tracks.at(ip3d_track_index))) {
      ip3d_grade = m_ip3d_gradeOfTracks(btagging).at(ip3d_track_index);
      break;
    }
  }
  m_ip3d_grade(track) = ip3d_grade;

  int ip2d_grade = -1;
  const std::vector<ElementLink<xAOD::TrackParticleContainer> > ip2d_tracks = m_ip2d_trackParticleLinks(btagging);
  for (std::size_t ip2d_track_index = 0; ip2d_track_index < ip2d_tracks.size(); ++ip2d_track_index) {
    if (&track == *(ip2d_tracks.at(ip2d_track_index))) {
      ip2d_grade = m_ip2d_gradeOfTracks(btagging).at(ip2d_track_index);
      break;
    }
  }
  m_ip2d_grade(track) = ip2d_grade;
}
