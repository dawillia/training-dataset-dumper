#ifndef BTAG_TRACK_AUGMENTER_HH
#define BTAG_TRACK_AUGMENTER_HH

#include <vector>

#include "AthContainers/AuxElement.h"
#include "AthLinks/ElementLink.h"

#include "xAODTracking/TrackParticleContainer.h"

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
}

class BTagTrackAugmenter {
  public:
    BTagTrackAugmenter();
    void augment(const xAOD::TrackParticle &track, const xAOD::Jet &jet);
  private:
    typedef SG::AuxElement AE;
  
    AE::ConstAccessor<float> m_ip_d0;
    AE::ConstAccessor<float> m_ip_z0;
    AE::ConstAccessor<float> m_ip_d0_sigma;
    AE::ConstAccessor<float> m_ip_z0_sigma;
    AE::ConstAccessor<std::vector<float> > m_track_displacement;
    AE::ConstAccessor<std::vector<float> > m_track_momentum;
    AE::ConstAccessor<std::vector<ElementLink<xAOD::TrackParticleContainer> > > m_ip2d_trackParticleLinks;
    AE::ConstAccessor<std::vector<ElementLink<xAOD::TrackParticleContainer> > > m_ip3d_trackParticleLinks;
    AE::ConstAccessor<std::vector<int> > m_ip2d_gradeOfTracks;
    AE::ConstAccessor<std::vector<int> > m_ip3d_gradeOfTracks;
    AE::Decorator<float> m_ip2d_signed_d0;
    AE::Decorator<float> m_ip3d_signed_d0;
    AE::Decorator<float> m_ip3d_signed_z0;
    AE::Decorator<float> m_ip3d_signed_d0_significance;
    AE::Decorator<float> m_ip3d_signed_z0_significance;
    AE::Decorator<int> m_ip2d_grade;
    AE::Decorator<int> m_ip3d_grade;
};

#endif
