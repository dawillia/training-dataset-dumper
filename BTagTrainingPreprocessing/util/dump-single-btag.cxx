#include <cstddef>
#include <memory>
#include <cmath>

#include "BTaggingWriterConfiguration.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagTrackWriterConfig.hh"

#include "BTagJetAugmenter.hh"
#include "BTagJetWriter.hh"
#include "BTagROOTJetFiller.hh"

#include "BTagTrackAugmenter.hh"
#include "BTagTrackWriter.hh"
#include "BTagROOTTrackFiller.hh"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODCore/tools/PerfStats.h"
#include "xAODCore/tools/IOStats.h"
#include "xAODCore/tools/ReadStats.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "H5Cpp.h"

#include "TFile.h"
#include "TTree.h"

namespace {
  bool is_from_W_or_Z(const xAOD::TruthParticle &truth_particle) {
    for (std::size_t parent_index = 0; parent_index < truth_particle.nParents(); parent_index++) {
      const xAOD::TruthParticle &parent = *truth_particle.parent(parent_index);
      if (parent.isW() || parent.isZ() || is_from_W_or_Z(parent)) return true;
    }
    return false;
  }
  
  bool is_overlaping_electron_or_muon(const xAOD::Jet &jet, const xAOD::TruthEventContainer &truth_events) {
    for (const xAOD::TruthEvent *const truth_event : truth_events) {
      for(std::size_t truth_event_index = 0; truth_event_index < truth_event->nTruthParticles(); truth_event_index++) {
        const xAOD::TruthParticle *const truth_particle = truth_event->truthParticle(truth_event_index);
  
        // protection for slimmed event record
        if ( ! truth_particle) continue;
  
        // now check if it's an overlaping election or muon
        if (! (truth_particle->isElectron() || truth_particle->isMuon())) continue;
        if (truth_particle->pt() < 10000 || truth_particle->status() != 1) continue;
        if (jet.p4().DeltaR(truth_particle->p4()) > 0.3) continue;
  
        // ...from a W or Z
        if (is_from_W_or_Z(*truth_particle)) return true;
      } // end of particle loop
    }   // end of truth events loop
    return false;
  }
}

int main (int argc, char *argv[]) {
  // The name of the application:
  const char *const APP_NAME = "BTagTestDumper";

  // Check that at least one input file was provided:
  if (argc < 2) {
    Error( APP_NAME, "Usage: %s <file1> [file2] ...", APP_NAME );
    return 1;
  }

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("JetCollection", "AntiKt4EMTopo") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("ConfigFile", "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("CalibArea", "00-04-81") );
  RETURN_CHECK( APP_NAME, calib_tool.setProperty("IsData", false) );
  RETURN_CHECK( APP_NAME, calib_tool.initialize() );

  JetCleaningTool jetcleaningtool("JetCleaningTool", JetCleaningTool::LooseBad, false);
  RETURN_CHECK( APP_NAME, jetcleaningtool.initialize() );

  InDet::InDetTrackSelectionTool indettrackselectiontool("InDetTrackSelectionTool", "Loose");
  RETURN_CHECK( APP_NAME, indettrackselectiontool.initialize() );

  JetVertexTaggerTool jvttool("JetVertexTaggerTool");
  RETURN_CHECK( APP_NAME, jvttool.initialize() );

  // this tool adds variables we need for b-tagging which are derived
  // from the jet
  BTagJetAugmenter jet_augmenter;

  BTagTrackAugmenter track_augmenter;

  // new way to do output files
  H5::H5File output("output.h5", H5F_ACC_TRUNC);
  // set up jet writer
  BTagJetWriterConfig jet_cfg;
  jet_cfg.write_event_info = true;
  cfg::insert_into(jet_cfg.char_variables, cfg::BTagAddedChars);
  cfg::insert_into(jet_cfg.truth_labels, cfg::BTagTruthLabels);
  cfg::insert_into(jet_cfg.jet_float_variables, cfg::BTagTruthFloats);
  cfg::insert_into(jet_cfg.int_as_float_variables, cfg::BTagInts);
  cfg::insert_into(jet_cfg.int_as_float_variables, cfg::BTagAddedInts);
  cfg::insert_into(jet_cfg.float_variables, cfg::BTagFloats);
  cfg::insert_into(jet_cfg.float_variables, cfg::BTagAddedFloats);
  cfg::insert_into(jet_cfg.float_variables, cfg::SMTFloats);
  cfg::insert_into(jet_cfg.double_variables, cfg::BTagDoubles);
  cfg::insert_into(jet_cfg.double_variables, cfg::BTagAddedDoubles);
  cfg::insert_into(jet_cfg.double_variables, cfg::SMTDoubles);
  jet_cfg.variable_maps.replace_with_defaults_checks = cfg::check_map_from(cfg::BTagDefaultsMap);
  jet_cfg.variable_maps.rename = {}; // please don't use this :(
  jet_cfg.name = "jets";
  BTagJetWriter jet_writer(output, jet_cfg);
  // set up track writer
  BTagTrackWriterConfig track_cfg;
  track_cfg.name = "tracks";
  track_cfg.uchar_variables = cfg::TrackUChars;
  track_cfg.int_variables = cfg::TrackInts;
  track_cfg.float_variables = cfg::TrackFloats;
  track_cfg.output_size = {10};
  BTagTrackWriter track_writer(output, track_cfg);

  TFile root_output_file("output.root", "recreate");
  if (root_output_file.IsZombie()) {
    Error(APP_NAME, "Unable to create ROOT output file");
    return 1;
  }

  TTree output_tree("FTagDumperTree", "FTagDumperTree");

  BTagROOTJetFiller root_jet_filler(output_tree, jet_cfg);
  BTagROOTTrackFiller root_track_filler(output_tree, track_cfg);

  // Start the measurement:
  auto ps = xAOD::PerfStats::instance();
  ps.start();

  // Loop over the specified files:
  for (int i = 1; i < argc; ++i) {
    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(argv[i], "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", argv[i] );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", argv[i] );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // Loop over its events:
    const Long64_t entries = event.getEntries();
    for (Long64_t entry = 0; entry < entries; ++entry) {

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s", entry, argv[i] );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }

      const xAOD::JetContainer *jets = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(jets, "AntiKt4EMTopoJets") );

      const xAOD::TruthEventContainer *truth_events = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(truth_events, "TruthEvents") );

      const xAOD::EventInfo *event_info = nullptr;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      for (const xAOD::Jet *const uncalib_jet : *jets) {
        std::unique_ptr<const xAOD::Jet> calib_jet(nullptr);
        xAOD::Jet *i_hate_you_asg(nullptr);
        calib_tool.calibratedCopy(*uncalib_jet, i_hate_you_asg);
        calib_jet.reset(i_hate_you_asg);

        if (is_overlaping_electron_or_muon(*calib_jet, *truth_events)) continue;

	float updated_jvt_value= jvttool.updateJvt(*calib_jet);
        if (calib_jet->pt() > 20000 && calib_jet->pt() < 60000 && std::abs(calib_jet->eta()) < 2.4 && updated_jvt_value < 0.59) continue;

        if ( ! jetcleaningtool.keep(*calib_jet)) continue;

        if (calib_jet->pt() < 20000 || std::abs(calib_jet->eta()) > 2.5) {
          continue;
        }

        jet_augmenter.augment(*calib_jet, *uncalib_jet);
        jet_writer.write(*calib_jet, event_info);
        root_jet_filler.fill(*calib_jet, event_info);

        // make a vector to store tracks to HDF5.
        std::vector<const xAOD::TrackParticle *> tracks;
        // Read the track particles:
        const xAOD::BTagging *const btagging = calib_jet->btagging();
        const auto links = btagging->auxdata<std::vector<ElementLink<xAOD::TrackParticleContainer> > >("BTagTrackToJetAssociator");
        for (const auto &link : links) {
            const xAOD::TrackParticle &tp = **link;
            if ( ! indettrackselectiontool.accept(&tp)) continue;
            track_augmenter.augment(tp, *calib_jet);
            tracks.push_back(&tp);
  //        }
        }
        track_writer.write(tracks, *calib_jet);
        root_track_filler.fill(tracks, *calib_jet);
      }
      output_tree.Fill();
    }
  }
  root_output_file.cd();
  output_tree.Write();
  // Stop the measurement:
  ps.stop();
  xAOD::IOStats::instance().stats().Print("Summary");
  return 0;
}
