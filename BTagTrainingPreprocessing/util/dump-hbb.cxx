/// @file dump-test.cxx
/// @brief Test reading the main calo cluster and track particle container
///
/// This test simply reads in the static payload of the track particle
/// container of a primary xAOD. And checks how fast this can actually
/// be done.

#include "HbbOptions.hh"

// new way to do local things
#include "BTagTrackWriterConfig.hh"
#include "BTagJetWriterConfig.hh"
#include "BTagJetWriter.hh"
#include "BTagTrackWriter.hh"
#include "BookKeeper.hh"
#include "addMetadata.hh"

#include "BTaggingWriterConfiguration.hh"

// System include(s):
#include <memory>
#include <string>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TLorentzVector.h>

// HDF includes
#include "H5Cpp.h"

// AnalysisBase tool include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "BoostedJetTaggers/HbbTaggerDNN.h"
#include "AsgTools/AnaToolHandle.h"

// EDM include(s):
#include "JetCalibTools/JetCalibrationTool.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthEventContainer.h"

class TrackSelector
{
public:
  TrackSelector();
  typedef std::vector<const xAOD::TrackParticle*> Tracks;
  Tracks get_tracks(const xAOD::Jet& jet) const;
private:
  typedef SG::AuxElement AE;
  typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
  AE::ConstAccessor<TrackLinks> m_track_associator;

  InDet::InDetTrackSelectionTool m_track_selector;
};

TrackSelector::TrackSelector():
  m_track_associator("BTagTrackToJetAssociator"),
  m_track_selector("InDetTrackSelectionTool", "Loose")
{
  if (!m_track_selector.initialize()) {
    throw std::logic_error("can't initialize track seletor");
  }
}

TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  const xAOD::BTagging *btagging = jet.btagging();
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto &link : m_track_associator(*btagging)) {
    if(link.isValid()) {
      const xAOD::TrackParticle *tp = *link;
      if (m_track_selector.accept(tp)) {
        tracks.push_back(tp);
      }
    } else {
      throw std::logic_error("invalid track link");
    }
  }
  return tracks;
}

// sort functions
bool by_d0(const xAOD::TrackParticle* t1,
           const xAOD::TrackParticle* t2) {
  static SG::AuxElement::ConstAccessor<float> d0("btag_ip_d0");
  return std::abs(d0(*t1)) > std::abs(d0(*t2));
}
bool pt_sort(const xAOD::Jet* sj1, const xAOD::Jet* sj2 ){
  return sj1->pt() > sj2->pt();
}


const double GeV = 1000;

int main (int argc, char *argv[])
{
  IOOpts opts = get_io_opts(argc, argv);
  // The name of the application:
  static const char *APP_NAME = "BTagTestDumper";

  // Other constants
  const std::string jet_collection = "AntiKt10LCTopoTrimmedPtFrac5SmallR20";
  const std::string jet_calib_file = "JES_MC16recommendation_FatJet_JMS_comb_19Jan2018.config";
  const std::string cal_seq = "EtaJES_JMS";
  const std::string cal_area = "00-04-81";

  const std::string subjet_collection = "GhostVR30Rmax4Rmin02TrackJet";
  const size_t jets_to_save = 3;

  // subjet accessors
  typedef SG::AuxElement AE;
  typedef ElementLink<xAOD::JetContainer> JetLink;
  AE::ConstAccessor<JetLink> acc_parent("Parent");
  typedef std::vector<ElementLink<xAOD::IParticleContainer> > ParticleLinks;
  AE::ConstAccessor<ParticleLinks> acc_subjets(subjet_collection);

  TrackSelector track_selector;

  // Set up the environment:
  RETURN_CHECK( APP_NAME, xAOD::Init() );

  // Set up the event object:
  xAOD::TEvent event(xAOD::TEvent::kClassAccess);

  // Initialize JetCalibrationTool with release 21 recommendations
  JetCalibrationTool calib_tool("JetCalibrationTool");
  calib_tool.setProperty("JetCollection", jet_collection);
  calib_tool.setProperty("ConfigFile", jet_calib_file);
  calib_tool.setProperty("CalibSequence", cal_seq);
  calib_tool.setProperty("CalibArea", cal_area);
  calib_tool.setProperty("IsData", false);
  RETURN_CHECK( APP_NAME, calib_tool.initialize() );

  InDet::InDetTrackSelectionTool indettrackselectiontool("InDetTrackSelectionTool", "Loose");
  RETURN_CHECK( APP_NAME, indettrackselectiontool.initialize() );

  // for some reason this tool was segfaulting without a tool
  // handle. No idea why...
  asg::AnaToolHandle<HbbTaggerDNN> tagger;
  tagger.setTypeAndName("HbbTaggerDNN","HbbTaggerDNN");
  // maybe setup a neural network
  if (opts.nn_file.size() > 0) {
    if (opts.verbose) tagger.setProperty("OutputLevel", MSG::DEBUG);
    tagger.setProperty("neuralNetworkFile", opts.nn_file);
    tagger.setProperty("decorationName", "HbbScore");
    RETURN_CHECK( APP_NAME, tagger.retrieve());
  }

  // define output file output files
  H5::H5File output(opts.out, H5F_ACC_TRUNC);

  // set up fat jet writer
  BTagJetWriterConfig fat_cfg;
  fat_cfg.write_substructure_moments = true;
  fat_cfg.write_event_info = true;
  fat_cfg.jet_float_variables = cfg::FatJetFloats;
  if (tagger.isInitialized()) {
    fat_cfg.jet_float_variables.push_back("HbbScore");
  }
  fat_cfg.parent_truth_labels = {"GhostHBosonsCount"};
  fat_cfg.name = "fat_jet";
  BTagJetWriter fatjet_writer(output, fat_cfg);

  // set up jet writer
  std::vector<BTagJetWriter> subjet_writer;
  BTagJetWriterConfig jet_cfg;
  jet_cfg.write_kinematics_relative_to_parent = true;
  jet_cfg.double_variables = cfg::BTagDoubles;
  jet_cfg.float_variables = cfg::BTagFloats;
  jet_cfg.int_variables = cfg::BTagInts;
  jet_cfg.truth_labels = cfg::BTagTruthLabels;
  for (size_t jetn = 0; jetn < jets_to_save; jetn++) {
    jet_cfg.name = "subjet" + std::to_string(1 + jetn);
    subjet_writer.emplace_back(output, jet_cfg);
  }

  // set up track writer
  std::vector<std::unique_ptr<BTagTrackWriter> > track_writer;
  BTagTrackWriterConfig track_cfg;
  track_cfg.float_variables = cfg::TrackFloats;
  track_cfg.uchar_variables = cfg::TrackUChars;
  track_cfg.output_size = {opts.n_tracks};
  for (size_t jetn = 0; jetn < jets_to_save; jetn++) {
    track_cfg.name = "subjet" + std::to_string(1 + jetn) + "_tracks";
    track_writer.emplace_back(new BTagTrackWriter(output, track_cfg));
  }

  // keep track of the events in the AOD
  Counts counts;

  // Loop over the specified files:
  for (const std::string& file: opts.in) {

    // Open the file:
    std::unique_ptr<TFile> ifile(TFile::Open(file.c_str(), "READ"));
    if ( ! ifile.get() || ifile->IsZombie()) {
      Error( APP_NAME, "Couldn't open file: %s", file.c_str() );
      return 1;
    }
    Info( APP_NAME, "Opened file: %s", file.c_str() );

    // Connect the event object to it:
    RETURN_CHECK( APP_NAME, event.readFrom(ifile.get()) );

    // Loop over its events:
    const unsigned long long entries = event.getEntries();
    if (entries > 0) {
      event.getEntry(0);
      counts += get_counts(*ifile, event);
    }
    for (unsigned long long entry = 0; entry < entries; ++entry) {
      if (opts.n_entries && entry > opts.n_entries) break;

      // Load the event:
      if (event.getEntry(entry) < 0) {
        Error( APP_NAME, "Couldn't load entry %lld from file: %s",
               entry, file.c_str() );
        return 1;
      }

      // Print some status:
      if ( ! (entry % 500)) {
        Info( APP_NAME, "Processing entry %lld / %lld", entry, entries );
      }
      const xAOD::EventInfo* event_info = 0;
      RETURN_CHECK( APP_NAME, event.retrieve(event_info, "EventInfo") );

      const xAOD::JetContainer *raw_jets = 0;
      auto full_collection = jet_collection + "Jets";
      RETURN_CHECK( APP_NAME, event.retrieve(raw_jets, full_collection) );

      for (const xAOD::Jet *raw_jet : *raw_jets) {
        std::unique_ptr<xAOD::Jet> jet(nullptr);
        xAOD::Jet* jet_ptr(nullptr);
        calib_tool.calibratedCopy(*raw_jet, jet_ptr);
        jet.reset(jet_ptr);
        if (jet->pt() < 250*GeV || std::abs(jet->eta()) > 2.0) {
          continue;
        }
        if (tagger.isInitialized()) tagger->decorate(*jet);

        // get the subjets
        const xAOD::Jet* parent_jet = *acc_parent(*jet);
        if (!parent_jet) throw std::logic_error("no valid parent");
        auto subjet_links = acc_subjets(*parent_jet);
        std::vector<const xAOD::Jet*> subjets;
        for (const auto& el: subjet_links) {
          const auto* jet = dynamic_cast<const xAOD::Jet*>(*el);
          if (!jet) throw std::logic_error("subjet is invalid");
          if (jet->pt() > 7e3) {
            subjets.push_back(jet);
          }
        }
        std::sort(subjets.begin(), subjets.end(), pt_sort);

        fatjet_writer.write_with_parent(*jet, *parent_jet, event_info);

        for (size_t jet_number = 0; jet_number < jets_to_save; jet_number++) {
          auto& sjwriter = subjet_writer.at(jet_number);
          auto& trkwriter = track_writer.at(jet_number);
          if (jet_number < subjets.size()) {
            auto& subjet = *subjets.at(jet_number);
            sjwriter.write_with_parent(subjet, *jet);
            auto tracks = track_selector.get_tracks(subjet);
            sort(tracks.begin(), tracks.end(), by_d0);
            trkwriter->write(tracks, subjet);
          } else {
            sjwriter.write_dummy();
            trkwriter->write_dummy();
          }
        } // end loop over subjets

      } // end jet loop

    } // end event loop
  } // end file loop

  // save count metadata
  addMetadata(output, counts);

  return 0;
}
