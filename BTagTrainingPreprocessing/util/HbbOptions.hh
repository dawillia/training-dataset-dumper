#ifndef HBB_OPTIONS_HH
#define HBB_OPTIONS_HH

#include <vector>
#include <string>

struct IOOpts
{
  std::vector<std::string> in;
  std::string out;
  size_t n_entries;
  std::string nn_file;
  bool verbose;
  size_t n_tracks;
};

IOOpts get_io_opts(int argc, char* argv[]);


#endif
