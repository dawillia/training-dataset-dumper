#include "HbbOptions.hh"
#include <boost/program_options.hpp>
#include <iostream>


IOOpts get_io_opts(int argc, char* argv[]) {
  IOOpts file;
  namespace po = boost::program_options;
  std::string usage = "usage: " + std::string(argv[0]) + " <files>..."
    + " -o <output> [-h] [opts...]\n";
  po::options_description opt(usage + "\nDump Hbb training HDF5 from an AOD");
  opt.add_options()
    ("in-file",
     po::value(&file.in)->required()->multitoken(),
     "input file name")
    ("out-file,o",
     po::value(&file.out)->default_value("output.h5"),
     "output file name")
    ("help,h", "Print help messages")
    ("n-entries,n",
     po::value(&file.n_entries)->default_value(0, "all"),
     "number of entries to copy")
    ("nn-file,t",
     po::value(&file.nn_file)->default_value(""),
     "neural network for higgs tagging")
    ("verbose,v", po::bool_switch(&file.verbose),
     "print more info")
    ("n-tracks,m", po::value(&file.n_tracks)->default_value(10),
     "number of tracks to save per subjet")
    ;

  po::positional_options_description pos_opts;
  pos_opts.add("in-file", -1);

  po::variables_map vm;
  try {
    po::store(po::command_line_parser(argc, argv).options(opt)
              .positional(pos_opts).run(), vm);
    if ( vm.count("help") ) {
      std::cout << opt << std::endl;
      exit(1);
    }
    po::notify(vm);
  } catch (po::error& err) {
    std::cerr << usage << "ERROR: " << err.what() << std::endl;
    exit(1);
  }
  return file;

}
