Flavor Tagging Ntuple Dumper
============================

This is to dump b-tagging info from an AnalysisBase release

To compile the code, go to a clean directory and run run:

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
mkdir build
cd build
source ../training-dataset-dumper/setup.sh
cmake ../training-dataset-dumper
make
```

Then to run a test (on a file which currently lives on AFS) you can use

```bash
../training-dataset-dumper/run.sh
```

This won't do much right now, but it will compile and test the program in

```text
BTagTrainingPreprocessing/utils/dump-single-btag.cxx
```

which is a minimal working example to read an xAOD and write some
information to HDF5.

Running on the grid
-------------------

From the directory where you checked out the package, after running
the `setup.sh` script above, run the following:

```bash
source training-dataset-dumper/grid/setup.sh
```

Then run

```bash
./training-dataset-dumper/grid/submit.sh
```

This should submit a grid test job. Note that it's still a work in
progress, take a look at the script to see how it works.

Package Layout
--------------

The code lives under `BTagTrainingPreprocessing`. All the top-level
executables live in `util/`, whereas various private internal classes
are defined in `src/`.

Other Tricks
------------

You can examine HDF5 files with `h5ls`. See `h5ls -h` for more
information. I also wrote a nice [tab-complete script for][1] to make
this even better. Download it and then source it in your `.bashrc`.

[1]: https://gist.githubusercontent.com/dguest/1453cf42a688525861610a9d3dcdf61d/raw/e1f8620c860b7567714c0893548aa417d5a26445/_h5ls.sh

Working document
----------------
See [here](https://docs.google.com/document/d/1i954ZC0ZX1NIc4t9K1sKhOXkjRRObpK9ANIPCjcqlvA/edit?usp=sharing)
